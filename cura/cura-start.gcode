G21 ; Set to metric values
G90 ; Absolute positioning
M82 ; Set extruder to absolute mode
M107 ; Start with the fan off

G28 X0 Y0 Z0 ; Home all axes to min endstops
; G29 ; if using BLTOUCH leveling

G0 Z2.0 ; Move Z Axis up little to prevent scratching the heatbed
G0 X5.0 Y8.0 ; Move XY to start position as fast as possible
G0 Z0.3 ; Move Z axis to start position as fast as possible
G92 E0 ; Reset Extruder

G1 X85.0 E20 F1000.0 ; Draw the purge line

G1 Z3.0 F1000.0 ; Move Z Axis up
G1 X90 F4000 ; Quickly wipe away from the filament line
G92 E0 ; Reset Extruder