M220 S100 ; Reset Speed factor to default (100%)
M221 S100 ; Reset Extrude factor to default (100%)

G91 ; Set RELATIVE positioning
G1 E-1 F300 ; Retract filament to prevent oozing
G1 E-2 Z2 F1000; Retract and lift
G1 X5 Y5 F3000 ; Wipe out
G1 Z8 ; Raise Z some more

; MOVE TO END POSITION
G90 ; Set ABSOLUTE positionning
G28 X0 ;move X to min endstops, so the head is out of the way
G1 X0 Y170 ; Present print

; THE END
M84 X Y E ;Disable all steppers but Z
M104 S0 ; Turn off extruder
M140 S0 ; Turn off bed
M106 S0 ; Turn off cooling fan
M107    ; Turn off Fan
M84     ; Turn off steppers