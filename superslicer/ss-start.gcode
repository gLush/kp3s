G21 ; Set to metric values
G90 ; Absolute positioning
M82 ; Set extruder to absolute mode
M107 ; Start with the fan off

M140 S[first_layer_bed_temperature] ;set table temperature
M104 S[first_layer_temperature]  ;set the nozzle temperature
M109 S[first_layer_temperature]  ;wait until the nozzle temperature is reached
M190 S[first_layer_bed_temperature] ;wait until the table temperature is reached
G21 ;metric values
G90 ;absolute positioning
SET_PRESSURE_ADVANCE EXTRUDER=extruder ADVANCE=0.045 SMOOTH_TIME=0.04
G28 X0 Y0 Z0 ; Home all axes to min endstops
G0 Z2.0 ; Move Z Axis up little to prevent scratching the heatbed
G0 X5.0 Y8.0 ; Move XY to start position as fast as possible
G0 Z0.3 ; Move Z axis to start position as fast as possible
G92 E0 ; Reset Extruder

G1 X85.0 E20 F1000.0 ; Draw the purge line

G1 Z3.0 F1000.0 ; Move Z Axis up
G1 X90 F4000 ; Quickly wipe away from the filament line
G92 E0 ; Reset Extruder